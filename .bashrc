#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PS1='\W/ '

# aliases
alias ls='ls --color=auto'
alias update='sudo pacman -Syu'
alias aclean='~/.arch-clean'
alias rex='xrdb ~/.Xresources'
alias search='pacman -Qi'
alias grubupdate='sudo grub-mkconfig -o /boot/grub/grub.cfg'